package io.eumes.university.pem2014.chuckme.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import io.eumes.university.pem2014.chuckme.R;
import io.eumes.university.pem2014.chuckme.api.ICNDbJokeService;
import io.eumes.university.pem2014.chuckme.api.ICNDbJokeServiceFactory;
import io.eumes.university.pem2014.chuckme.api.message.ICNDbResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ChuckMeActivity extends Activity {

    private ICNDbJokeService jokeService;

    private TextView jokeDisplay;
    private Button jokeQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //reload saved state if activity was destroyed while being in the background
        super.onCreate(savedInstanceState);

        //set the layout for our activity
        setContentView(R.layout.activity_chuckme);

        //wire up the button and the textview
        jokeQuery = (Button)findViewById(R.id.jokeQuery);
        jokeDisplay = (TextView)findViewById(R.id.jokeDisplay);

        //we initialize the rest service
        jokeService = ICNDbJokeServiceFactory.buildJokeService();

        //and finally wire the service up to a button click
        setupJokeQuery();
    }

    private void setupJokeQuery(){

        //set a listener for the button click
        jokeQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //request a new joke (this is done asynchronously and the callback is called on the main thread (retrofit default)
                jokeService.randomJoke(new Callback<ICNDbResponse>() {
                    @Override
                    public void success(ICNDbResponse icnDbResponse, Response response)
                    {
                        String joke = icnDbResponse.value.joke;
                        jokeDisplay.setText(joke);
                    }

                    @Override
                    public void failure(RetrofitError retrofitError)
                    {
                        jokeDisplay.setText("Ummmh, where is Chuck?...\nHelp save the Internet");
                    }
                });
            }
        });
    }

}
