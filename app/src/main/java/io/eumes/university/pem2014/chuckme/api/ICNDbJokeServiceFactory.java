package io.eumes.university.pem2014.chuckme.api;

import retrofit.RestAdapter;

public class ICNDbJokeServiceFactory
{

    /**
     * Factory method building the retrofit adapter for the Internet Chuck Norris Database
     * @return The new rest adapter service to use for queries
     */
    public static ICNDbJokeService buildJokeService()
    {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://api.icndb.com")
                .build();

        ICNDbJokeService service = restAdapter.create(ICNDbJokeService.class);
        return service;
    }

}
