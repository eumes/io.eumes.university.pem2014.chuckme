package io.eumes.university.pem2014.chuckme.api.message;

/**
 * Simple wrapper object for the complete API response.
 */
public class ICNDbResponse {

    public String type;
    public ICNDbJoke value;

}
