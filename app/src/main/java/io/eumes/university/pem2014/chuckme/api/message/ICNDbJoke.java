package io.eumes.university.pem2014.chuckme.api.message;

/**
 * Simple object encapsulating each response item from the API.
 */
public class ICNDbJoke {

    public String id;
    public String joke;
    public String[] categories;
}
