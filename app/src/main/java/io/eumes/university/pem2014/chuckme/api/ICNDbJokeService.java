package io.eumes.university.pem2014.chuckme.api;

import io.eumes.university.pem2014.chuckme.api.message.ICNDbResponse;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Interface describing the Internet Chuck Norris Database API endpoint for retrofit
 */
public interface ICNDbJokeService
{
    @GET("/jokes/random")
    public void randomJoke(Callback<ICNDbResponse> callback);
}
